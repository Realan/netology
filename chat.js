const EventEmitter = require('events');

class ChatApp extends EventEmitter {
  /**
   * @param {String} title
   */
  constructor(title) {
    super();

    this.title = title;
    this.status = "ping-pong";

    // Посылать каждую секунду сообщение
    setInterval(() => {
      this.emit('message', `${this.title}: ${this.status}`);
    }, 1000);
  }
}

    // 2.1. В классе чата ChatApp добавить метод close, который будет вызывать событие close 
    // (событие вызывается один раз, setInterval как в констукторе, не нужен).
var eventName = "closeMe";
ChatApp.prototype.close = function(data){
    this.emit(eventName, data);
}

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk')
    .setMaxListeners(2);    // 1.2. Для Вконтакте (vkChat) установить максимальное количество 
                            //      обработчиков событий, равное 2
let vkChat1 =      new ChatApp('---------vk');

let chatOnMessage = (message) => {
  console.log(message);
};


webinarChat.on('message', chatOnMessage);

//1.1. Добавить обработчик события message для Чата Вебинара (webinarChat),
//  который выводит в консоль сообщение 'Готовлюсь к ответу'. 
//  Обработчик создать в отдельной функции.
webinarChat.on('message', status = () => {  
  console.log("  Готовлюсь к ответу");         
});       

facebookChat.on('message', chatOnMessage);

vkChat.on('message', chatOnMessage);

// 1.3 Добавить обработчик 'Готовлюсь к ответу' из пункта 1.1 к чату Вконтакте.
vkChat.on('message', status);  

// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.removeListener('message', chatOnMessage);
  vkChat.removeListener('message', status);
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  facebookChat.removeListener('message', chatOnMessage);
  facebookChat.close("hgkjhgjkghgkjhgkjhgkjhgkjhgkjhgkjhgkjhgkj");
}, 5000 );

/*

console.log("vk", vkChat.listeners('message'));
console.log("web", webinarChat.listeners('message'));
console.log("fb", facebookChat.listeners('message'));
console.log("vk1", vkChat1.listeners('close'));

console.log("eventMames vk", vkChat.eventNames());
console.log("eventMames vk1", vkChat1.eventNames());
console.log("eventMames webinarChat", webinarChat.eventNames());
console.log("eventMames facebookChat", facebookChat.eventNames());
*/

vkChat1.once('close', () => {
  console.log("Закрываюсь - Close")
});

exports.ChatApp = ChatApp;